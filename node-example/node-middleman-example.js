'use strict';

var http = require('http');
var url = require('url');
var fs = require('fs');

http.globalAgent.maxSockets=10000;

function route(req,res) {

	//extremely simple url based api
	var inUrl = url.parse(req.url, true);
	var command = inUrl.pathname.substr(1);

	if (command) {

		//jsonp
		if(inUrl.query.callback === undefined || inUrl.query.callback === '') {
			inUrl.query.callback = 'callback';
		}

		switch(command){
			case 'systemA' : {

				// data used for remote call
				var options = {
					host:'remote-host.com',
					port:80,
					path:'/get/me/my/data',
					headers:{
						'X-WTF':true
					}
				};

				// start building what we will return from our 
				var responseData = inUrl.query.callback + '(';

				// make our remote call
				var remoteRequest = http.get(options, function(r) {
					r.on('data',function(chunk){

						// add data coming in from our remote call
						responseData += chunk;
					});
					r.on('end', function() {

						// this completes what we will send back as a response from our initial call.
						res.end(responseData + ');');
					});
				}).on('error', function() {
					res.end(responseData + ');');
				});

				remoteRequest.setSocketKeepAlive(false);

				remoteRequest.setTimeout(15000, function() {
					res.end(responseData + ');');
					remoteRequest.abort();
				});

				return;
			}
			case 'static' : {
				// in this example, we are passing the file name we want loaded as a URL parameter, i.e: http://x.com/static?filename=my.js
				if (inUrl.query.filename) {
					// the way this is setup, the file would need to be in the same directory as this file itself is.
                    var l = fs.readFileSync(__dirname + '/' +  inUrl.query.filename, 'utf8', function (err) {
                        if (err) {
                            console.log(err);
                        }
                    });
 
                    res.end(l);
                    return;
                }
				break;
			}
		
			default:{
				break;
			}
		}
	}

	res.end('{}');
}

var d=require('domain').create();

d.on('error',function(e){
	console.log(e);
});

d.run(function(){
	http.createServer(function(req,res){
		res.writeHead(200, {'Content-Type':'application/json'});
		route(req,res);
	}).listen(4321, '0.0.0.0');
});

console.log('Server running on port 4321');