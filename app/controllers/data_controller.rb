class DataController < ApplicationController
	skip_before_action :verify_authenticity_token
	def index

	@data = User.iar_list()
	render :json => @data, :callback => params[:callback] 
	end


	def ofiprojects
	@projects = Idea.get_project_info('Office for Innovation Project Intake', 'Which strategic intent does this project fall under?')
	render :json => @projects, :callback => params[:callback]
	end


end
