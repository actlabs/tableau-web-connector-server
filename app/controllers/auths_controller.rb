class AuthsController < ApplicationController
	
	def index
		
		@tokens = Auth.generate_token
		session[:access_token] = @tokens["access_token"]
		
		redirect_to '/data#index'
			#end
	end
end
