class User < ActiveRecord::Base

	def self.iar_list()
		token_response = Auth.generate_token
		access_token = token_response["access_token"]
		users = Idea.get_info(access_token, "member")#HTTParty.get("https://act.brightidea.com/api3/member", :query => {"last_name" => "Smith", "page_size" => "100"}, :headers => {"Authorization" => "Bearer "+ access_token})
		ideas = Idea.get_info(access_token, "idea")
		comments = Idea.get_info(access_token, "comment")
		votes= Idea.get_info(access_token, "vote")
		#campaigns = Idea.get_info(access_token, "campaign")
		nuserlist=[]
		users.each do |user|
			user["flag"]=0 #To filter people with no activity
			user["CampaignList"]=[] #Initialize empty campaign list
			user = User.countprop(ideas,user, "ideacounts") #Fetch idea counts by campaign
			user = User.countprop(comments,user, "commentcounts")#Fetch comment counts by campaign
			user = User.countprop(votes,user, "votecounts")#Fetch vote counts by campaign

			
			nuserlist = User.createuserlist(nuserlist,user,user["flag"]) #Create a list of users with relevant fields
			
		end
		return nuserlist
	end

private
	def self.countprop(property,user,var)
		property.each do |el|
				if !(var=="ideacounts") #Defining the key by which the campaign of an activity can be
					#obtained from the API Response object. By the Brightidea design, "ideas" are structured
					#differently than comments and votes
					camp_key= el["idea"]["campaign"]["name"]
				else
					camp_key = el["campaign"]["name"]
				end
				if(user["id"] == el["member"]["id"]) #If the user id matches the one in the activity
					if !(user.key?(camp_key))
						user["CampaignList"].push(camp_key) #Maintain a list of the campaigns that the
						#user has participated in.
						user[camp_key] = {} #Initialize empty hash for the key, i.e the name of the campaign
					end
					if !(user[camp_key].key?(var))
						user[camp_key][var] =1	#If this is the first activity in the campaign by the user, initialize it to 1
					else
						user[camp_key][var] +=1 #Else increment
					end
					user["flag"]=1 #Set the flag indicating that the user has participated in some activity
				end
			end
		return user
	end

	def self.createuserlist(nuserlist,user, flag)
		if(flag==1) #If the user has participated in some activity
			user["CampaignList"].each do |camp| #For all the campaigns the user has participated in
			hash = {"ID": user["id"],
				"FirstName": user["first_name"],
			 "LastName": user["last_name"],
			 "ScreenName": user["screen_name"],
			 "Email": user["email"],
			 "Campaign": camp, #Set Campaign
			  "Ideas": user[camp]["ideacounts"] || 0,  #Idea counts for the campaign
			  "Comments": user[camp]["commentcounts"] || 0, #then user[camp]["commentcounts"] else 0 end, #Comment counts by campaign
			  "Votes": user[camp]["votecounts"] || 0,#then user[camp]["votecounts"] else 0 end, #Vote counts by campaign
			  "Title": user["job_title"],
			  "ExtId": user["external_user_id"]
			}
			nuserlist.push(hash)
			end
		else #If the user has no activity, set campaign to blank and other counts to 0
			hash = {"ID": user["id"],
				"FirstName": user["first_name"],
			 "LastName": user["last_name"],
			 "ScreenName":user["screen_name"],
			  "Email": user["email"],
			 "Campaign": "",
			  "Ideas": 0, 
			  "Comments": 0, 
			  "Votes": 0,
			  "Title": user["job_title"],
			  "ExtId": user["external_user_id"]
			}
			nuserlist.push(hash)
		end
		return nuserlist
	end
end

