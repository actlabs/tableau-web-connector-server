class Auth < ActiveRecord::Base
	def self.generate_token
		url = "https://auth.brightidea.com/_oauth2/token"
		request_data = { "grant_type" => "password",
		"client_id" =>Rails.application.secrets.client_id,
		"client_secret" => Rails.application.secrets.client_secret,
		"username" => Rails.application.secrets.username,
		"password" =>  Rails.application.secrets.password
		}
		@response_data = HTTParty.post(url, :body => request_data)
		puts(@response_data)
		return(@response_data)
	end


end
