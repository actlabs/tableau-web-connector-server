class Idea < ActiveRecord::Base


	def self.usercounts(access_token)
		ideas = HTTParty.get("https://act.brightidea.com/api3/member", :query => {"page_size" => "100"}, :headers => {"Authorization" => "Bearer "+ access_token})
		return(ideas["idea_list"])

	end

	def self.get_info(access_token, keyword)

		page = 0
		ideas={}
		q=""
		if (keyword == "member")
			q = ""
		else
			q = "date_created > '2015/08/01'"
		end
		idealist=[]
		while(true)

			page=page+1
			ideas = HTTParty.get("https://act.brightidea.com/api3/" + keyword, :query => { "page_size" => "100", "page" => page}, :headers => {"Authorization" => "Bearer "+ access_token, 'q' => q})
			idealist= idealist + ideas[keyword + "_list"]
			break if(ideas["stats"]["page_count"] == ideas["stats"]["current_page"])
		end
		return(idealist)
	end

	def self.get_project_info(campaign_name, addlquestion) #New function to get answers to questions
		token_response = Auth.generate_token
		access_token = token_response["access_token"]
		projectlist = Idea.get_project_details(campaign_name)
		puts(projectlist.length)
		newprojectlist = []

		#Get answer to the addlquestion asked
		#Currently it is designed to answer only a single question
		#However, it can be easily extended to get multiple answers
		projectlist.each do |pr|
			projectinfo = HTTParty.get("https://act.brightidea.com/api3/idea/" + pr[:ID], :query => { "with" => "additional_questions"}, :headers => {"Authorization" => "Bearer "+ access_token})
			projectinfo["idea"]["additional_questions"].each do |respquestion| #Find our question in the response items
				if(respquestion["description"] == addlquestion)
					pr["StrInt"] = respquestion["response_text"] #Answer to the "strategic intent question". Hardcoded for now, variable name should be changed
					break
				end
			end
			newprojectlist = newprojectlist << pr
			
		end
		puts(newprojectlist.length)
		return(newprojectlist)
	end
private
	def self.get_project_details(campaign_name) #Get information on each Project/Idea
	token_response = Auth.generate_token
	access_token = token_response["access_token"]
	page = 0
	projects={}
	project_id = Idea.get_campaign_id(campaign_name,access_token)
	
	
	projectlist=[]
	while(true)

		page=page+1
		projects = HTTParty.get("https://act.brightidea.com/api3/idea", :query => { "page_size" => "100", "page" => page}, :headers => {"Authorization" => "Bearer "+ access_token})
		projects["idea_list"].each do |pr|
			if pr["campaign"]["id"]== project_id #Insert in array if idea in given campaign
				project_info={
					"ID": pr["id"],
					"DateCreated": pr["date_created"],
					"Title": pr["title"],
					"Description": pr["description"],
					"BloomId": pr["idea_code"],
					"PostedBy": pr["member"]["screen_name"],
					"Status": pr["status"]["name"],
					"CategoryId": pr["category"]["id"],
					"CategoryName": pr["category"]["name"]

				}
				projectlist= projectlist<<project_info 
			end
		end
		break if(projects["stats"]["page_count"] == projects["stats"]["current_page"])
	end
	return(projectlist)
	end


	
		def self.get_campaign_id(name,access_token)
			campaigns={}
			campaignlist=[]
			q = "name = '" + name + "'"
			campaigns = HTTParty.get("https://act.brightidea.com/api3/campaign", :query=> {"page_size" => "10", "page" => 1}, :headers => {"Authorization" => "Bearer "+ access_token, 'q' => q})
			return campaigns["campaign_list"][0]["id"]
		end
	


end
